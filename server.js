const express = require('express')
var bodyParser = require('body-parser')
var passport = require('passport')
var session = require('express-session')
var cookieParser = require('cookie-parser')
var flash = require('connect-flash')
var app = express()

// Configure html paths
var path = require('path')
app.use(express.static(path.join(__dirname, 'public')))

// Routes
var api = require('./routes/api/api')
var db = require('./models/models')

var port = process.env.PORT || 3000

// var enrollment = db.Enrollment.findById(1)
//     .then( (enrollment) => {
//        var record = db.AttendenceRecord.build({
//           enrollment: enrollment.id
//        })
//        record.save().then((saved) => {
//           console.log(saved)
//        })
//    })


// Configure body parser
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))

//Configure cookie parser
app.use(cookieParser())

//Configure session
app.use(session({secret: '34khskd24fhskhf',
	resave: false, saveUninitialized: false}))
// Configure passport

app.use(passport.initialize())
app.use(passport.session())

app.use(flash())

// Initialize Passport
var initPassport = require('./passport/init')
initPassport(passport)

app.use(function(req, res, next) {
   res.header('Access-Control-Allow-Origin', '*')
   res.header('Access-Control-Allow-Headers',
              'Origin, X-Requested-With, Content-Type, Accept')
   next()
})

// This Should be in its own index file and setup with
//  'app.use()' like done for the api
app.get('/', function(req, res) {
   res.json({
      message: 'hooray! welcome to our api!'
   })
})

app.use('/api', api)

require('./routes/authenticate')(app, passport)

app.listen(port)
console.log(`Test with requests to port ${port}`)
