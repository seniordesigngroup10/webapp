-- -----------------------------------------------------
-- Table Attendant
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Attendant (
       idAttendant INT NOT NULL,
       name VARCHAR(45) NULL,      
       PRIMARY KEY (idAttendant));

-- -----------------------------------------------------
-- Table Proctor
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Proctor (
  idProctor INT NOT NULL,
  name VARCHAR(45) NULL,
  PRIMARY KEY (idProctor));

-- -----------------------------------------------------
-- Table Basestation
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Basestation (
  idBasestation SERIAL,
  room VARCHAR(45) NULL,
  PRIMARY KEY (idBaseStation));


CREATE TABLE IF NOT EXISTS login (
  idlogin     SERIAL,
  name        VARCHAR(45) NULL,
  password    VARCHAR(45) NULL, 
  attendant   INT         NULL DEFAULT NULL  REFERENCES Attendant(idAttendant)
                                               ON UPDATE CASCADE,
  proctor     INT         NULL DEFAULT NULL  REFERENCES Proctor(idProctor)
                                               ON UPDATE CASCADE,
  PRIMARY KEY(idlogin));

-- -----------------------------------------------------
-- Table Event
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Event (
  idEvent      SERIAL,
  name         VARCHAR(45)  NULL,
  daysOfWeek   VARCHAR(45)  NULL,
  beginTime    TIME         NULL,
  durration    INT          NULL,
  proctor      INT          NULL DEFAULT 0  REFERENCES Proctor(idProctor)
                                              ON DELETE NO ACTION
                                              ON UPDATE CASCADE,
  basestation  INT          NULL   REFERENCES Basestation(idBasestation)
                                         ON DELETE RESTRICT
                                         ON UPDATE CASCADE,
  PRIMARY KEY (idEvent));

-- -----------------------------------------------------
-- Table Enrollment
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Enrollment (
  idEnrollment  SERIAL,
  attendant     INT      NOT NULL  REFERENCES Attendant(idAttendant)
                                     ON DELETE SET DEFAULT
                                     ON UPDATE CASCADE,
  event         INT      NOT NULL  REFERENCES Event(idEvent)
                                     ON DELETE RESTRICT
                                     ON UPDATE CASCADE,
  PRIMARY KEY (idEnrollment));

-- -----------------------------------------------------
-- Table AttendenceRecord
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS AttendenceRecord (
  idAttendenceRecord  SERIAL,
  time                TIMESTAMP  NOT NULL,
  enrollment          INT        NOT NULL  REFERENCES Enrollment(idEnrollment)
                                             ON DELETE CASCADE
                                             ON UPDATE CASCADE,
  PRIMARY KEY (idAttendenceRecord));



