var Sequelize = require('sequelize')

var sequelize = new Sequelize(process.env.DATABASE_URL)

var Attendant = sequelize.define('attendant', {
   id: {
      type: Sequelize.INTEGER,
      field: 'idattendant',
      primaryKey: true
   },
   name: Sequelize.STRING(45)
}, {freezeTableName: true,
     timestamps: false})

var Basestation = sequelize.define('basestation', {
   id: {
      type: Sequelize.INTEGER,
      field: 'idbasestation',
      autoIncrement: true,
      primaryKey: true
   },
   room: Sequelize.STRING(45),
   uuid: Sequelize.TEXT,
   isActive: {
      type: Sequelize.BOOLEAN,
      field: 'isactive'
   }
}, {freezeTableName: true,
     timestamps: false})

var Proctor = sequelize.define('proctor', {
   id: {
      field: 'idproctor',
      type: Sequelize.INTEGER,
      primaryKey: true
   },
   name: Sequelize.STRING(45)
}, {freezeTableName: true,
    timestamps: false})

var Event = sequelize.define('event', {
   id: {
      field: 'idevent',
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true
   },
   name: Sequelize.STRING(45),
   daysofweek: Sequelize.STRING(45),
   time: {
      field: 'begintime',
      type: Sequelize.TIME
   },
   durration: Sequelize.INTEGER
}, {freezeTableName: true,
     timestamps: false})

Event.belongsTo(Proctor,{
   as: 'ProctorModel',
   foreignKey: 'proctor',
   onDelete: 'SET DEFAULT',
   onUpdate: 'CASCADE'
})

Proctor.hasMany(Event,{
   foreignKey: 'proctor',
   onDelete: 'SET DEFAULT',
   onUpdate: 'CASCADE'
})

Event.belongsTo(Basestation,{
   as: 'BasestationModel',
   foreignKey: 'basestation',
   onDelete: 'SET NULL',
   onUpdate: 'CASCADE'
})

Basestation.hasMany(Event,{
   foreignKey: 'basestation',
   onDelete: 'SET NULL',
   onUpdate: 'CASCADE'
})

var User = sequelize.define('login', {
   id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      field: 'idlogin',
      primaryKey: true
   },
   name: {
      type: Sequelize.STRING(45),
      allowNull: false,
      unique: true,
      validate: {
         isEmail: true
      }
   },
   password: Sequelize.STRING(45),
}, {freezeTableName: true,
    timestamps: false})

Attendant.hasOne(User,{
   foreignKey: 'attendant'
})

Proctor.hasOne(User, {
   foreignKey: 'proctor'
})


var Enrollment = sequelize.define('enrollment', {
   id: {
      field: 'idenrollment',
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true
   }
},{
   freezeTableName: true,
   timestamps: false
})

Enrollment.belongsTo(Event,{
   as: 'EventModel',
   foreignKey: 'event',
   onDelete: 'RESTRICT',
   onUpdate: 'CASCADE'
})

Event.hasMany(Enrollment,{
   foreignKey: 'event',
   onDelete: 'RESTRICT',
   onUpdate: 'CASCADE'
})


Attendant.hasMany(Enrollment,{
   foreignKey: 'attendant',
   onDelete: 'CASCADE',
   onUpdate: 'CASCADE'
})

var AttendenceRecord = sequelize.define('attendencerecord', {
   id: {
      field: 'idattendencerecord',
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true
   },
   time: Sequelize.DATE
}, {freezeTableName: true,
     timestamps: false})

Enrollment.hasMany(AttendenceRecord, {
   foreignKey: 'enrollment',
   onDelete: 'CASCADE',
   onUpdate: 'CASCADE'
})


module.exports = {
   sequelize:sequelize,
   Attendant: Attendant,
   Basestation: Basestation,
   Proctor: Proctor,
   Event: Event,
   User: User,
   Enrollment: Enrollment,
   AttendenceRecord: AttendenceRecord
}
