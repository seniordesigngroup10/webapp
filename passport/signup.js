var Model = require('../models/models')
var bCrypt = require('bcrypt-nodejs')
var LocalStrategy = require('passport-local').Strategy;

module.exports = function(passport){

	passport.use('signup', new LocalStrategy({
            passReqToCallback : true // allows us to pass back the entire request to the callback
        },
        function(req, username, password, done) {
        	Model.User.findAll({ where:{
				'name': username
				} 
			}).then(function(exists){
				if (exists[0] == null){
					var newUser ={
						name: username,
						password: password,

					}

					var attendant = Model.Attendant.findOrCreate({
						where:{
						id: req.body.id,
						name: req.body.first + ' ' + req.body.last
					}})

					 var user = Model.User.create(newUser).then(function(user){
					 	user.updateAttributes({attendant: req.body.id})
						return done(null, user)
					}).catch(function(error){
						throw error
					})
				
				}
			})
        })
	);
	var createHash = function(password){
		return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null)
	}
}