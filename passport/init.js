var login = require('./login')
var signup = require('./signup')
var Model = require('../models/models')


module.exports = function(passport){
	passport.serializeUser(function(user, done){
		done(null, user)
	})
	passport.deserializeUser(function(user, done) {
		console.log(user.name)
	    Model.User.findOne({
	      where: {
	        'name': user.name
	      }
	    }).then(function (user) {
	      if (user == null) {
	        done(new Error('Wrong user name.'))
	      }
	      
	      done(null, user)
	    })
	})
	login(passport)
	signup(passport)
}