var Model = require('../models/models')
var bCrypt = require('bcrypt-nodejs')
var LocalStrategy = require('passport-local').Strategy;

module.exports = function(passport){
	passport.use('login', new LocalStrategy(
		{passReqToCallback: true},
	function(req, username, password, done){
		Model.User.findOne({
			where:{
				'name': username
			}
		}).then(function(user) {
			if (user == null){
				return done(null, false, req.flash('message', 'Incorrect credentials.'))
			}
			//var hashedPassword = bcrypt.compareSync(password, user.password)
			if (password != user.password){
				return done(null, false, req.flash('message', 'Incorrect credentials.'))
			}
			
			return done(null, user)
		})
	}))
}
