console.log('Entered events.js');
angular.module('nodeEvent', [])
.controller('mainController', ($scope, $http) => {
  $scope.event = [];
  // Get all events
  $http.get('/eventsForAttendant')
  .success((event) => {
    $scope.event = event.event;
    console.log(event.event);
  })
  .error((error) => {
    console.log('Error in events.js: ' + error);
  });
});
