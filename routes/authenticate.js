var path = require('path');
var db = require('../models/models');

module.exports = function(app, passport){

	var isAuthenticated = function (req, res, next) {
	// if user is authenticated in the session, call the next() to call the next request handler 
	// Passport adds this method to request object. A middleware is allowed to add properties to
	// request and response objects
	if (req.isAuthenticated())
		return next();
	// if the user is not authenticated then redirect him to the login page
	console.log("Not authenticated")
	res.redirect('/');
	}

	/* GET login page. */
	app.get('/login', function(req, res) {
    	// Display the Login page with any flash message, if any
		res.sendFile('login.html', { root: path.join(__dirname, '../public') })
	});

	/* Handle Login POST */
	app.post('/login', passport.authenticate('login', {
		successRedirect: '/home',
		failureRedirect: '/login',
		failureFlash : true  
		})
	);

	/* GET Registration Page */
	app.get('/signup', function(req, res){
		res.sendFile('signup.html', { root: path.join(__dirname, '../public') })
	});

	/* Handle Registration POST */
	app.post('/signup', passport.authenticate('signup', {
		successRedirect: '/home',
		failureRedirect: '/signup',
		failureFlash : true  
	}));

	/* GET Home Page */
	app.get('/home', isAuthenticated, function(req, res){
		res.sendFile('home.html', { root: path.join(__dirname, '../public') })
	});

	/* Handle Logout */
	app.get('/signout', function(req, res) {
		req.logout();
		res.redirect('/');
	});

	app.get('/eventsForAttendant', function(req, res){
		db.Enrollment.findAll({
			where: {
				'attendant': req.session.passport.user.attendant	
			}
			}).then((eventIDs) =>{
				var i = 0;
				while (i < eventIDs.length){
					eventIDs[i] = eventIDs[i].dataValues.id
					i++
				}
				db.Event.findAll({
					where: {
						'idevent': eventIDs
					}
				})
				.then((event) =>{
					res.json({
						event
					})
				}).catch((e) => {
					console.error(e)
					res.status(500)
				})
		})
	})

}

