var express = require('express')
var endpoints = require('./endpoints')
var router = express.Router()

router.get('/attendant/:id', endpoints.getAttendant)

router.get('/attendant/:id/attendencerecord', endpoints.getAttendenceRecord)

router.get('/enrollment/schedule/:id', endpoints.getSchedule)

router.get('/basestation/:id/newUUID',
           endpoints.getBasestationCode)

router.post('/attendant/:id/validateAttendence', endpoints.validateAttendence)
module.exports = router
