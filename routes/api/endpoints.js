var uuid = require('uuid')

var moment = require('moment')
require('twix')

var db = require('../../models/models')

module.exports = {
   getAttendant: getAttendant,
   getBasestationCode: getBasestationCode,
   getSchedule: getSchedule,
   getAttendenceRecord: getAttendenceRecord,
   validateAttendence: validateAttendence
}


function validateAttendence(req,res,next){
   var attendID = parseInt(req.params.id)
   var enrollID = parseInt(req.query.enrollment.id)
   var uuid = req.query.uuid

   db.Enrollment.findById(
     enrollID,
     {
        include: [{
           model: db.Event,
           as: 'EventModel',
           include: [
             {
                model: db.Proctor,
                as: 'ProctorModel'
             },
             {
                model: db.Basestation,
                as: 'BasestationModel',
                attributes: ['id', 'room', 'isActive','uuid']
             }
           ]
        }]
     })
    .then((enrollment) => {
       if (isValid(enrollment.EventModel,uuid)){
          var record = db.AttendenceRecord.build({
             enrollment: enrollment.id
          })
          record.save()
           .then( (record) => {
              res.status(200)
               .json({
                  status: 'success',
                  message: 'attendance sucessfully recorded!',
                  timestamp: record.time
               })
           })
       } else {
          res.status(403)
           .json({
              status: 'failure',
              message: 'invalid attempt at attendance'
           })
       }
    })
}

function isValid(event, uuid){
   var basestationUUID  = event.BasestationModel.uuid
   var days = event.daysofweek
   var now = moment()
   var start = parseStart(event.time)
   var end = parseStart(event.time)
   end.add(event.durration, 'm')
   var range = start.twix(end)

   return (correctDayOfWeek(days,now.day) &&
           range.isCurrent() &&
           uuid === basestationUUID)
}

//Time in the format: HH:MM:SS
function parseStart(time){
   var temp  = time.split(':')
       .map((unit) => parseInt(unit))

   return moment({hour: temp[0], minute: temp[1]})
}

const DAYS = {u:0, m:1, t:2, w:3, r:4, f:5, s:6}


//Checks to see if a list of days contains the supplied date
function correctDayOfWeek(daysofweek, day){
   var i = daysofweek.length
   while (i--){
      if (DAYS[daysofweek[i]] === day) {
         return true
      }
   }
   return false
}


function getAttendenceRecord(req,res,next){
   var attendID = parseInt(req.params.id)
   var numResults = req.query.num ? parseInt(req.query.num) : 10
   var enrollID = parseInt(req.query.enrollment.id)

   db.Enrollment.findById(enrollID)
    .then( (enrollment) => {
       enrollment.getAttendencerecords(
         {order: [['time', 'DESC']],
          limit: numResults})
        .then((records) => {
           res.status(200)
            .json({
               status: 'success',
               records: records,
               message: 'Retrieved records'
            })
        })
    })
}


function getSchedule(req,res,next){
   var attendID = parseInt(req.params.id)
   db.Attendant.findById(attendID)
    .then( (attendant) => {
       attendant.getEnrollments({
          include: [{
             model: db.Event,
             as: 'EventModel',
             include: [{
                model: db.Proctor,
                as: 'ProctorModel'
             },{
                model: db.Basestation,
                as: 'BasestationModel',
                attributes: ['id', 'room', 'isActive']
             }]
          }],
          attributes: ['id','event']
       }).then( (enrollments) =>{
          var toRet = enrollments
              .map( (enrollment)=>{
                 enrollment.dataValues.event = enrollment.dataValues.EventModel

                 delete enrollment.dataValues.EventModel

                 enrollment.dataValues.event.dataValues.proctor =
                  enrollment.dataValues.event.ProctorModel
                 delete enrollment.dataValues.event.dataValues.ProctorModel

                 enrollment.dataValues.event.dataValues.basestation =
                  enrollment.dataValues.event.dataValues.BasestationModel

                 delete enrollment.dataValues.event.dataValues.BasestationModel
                 return enrollment
              })
          res.status(200)
           .json({
              status: 'success',
              schedule: toRet,
              message: 'Retrieved Student Schedule'
           })
       })
    }).catch( (e) => {
       console.log(e)
       next(e)
    })
}

function getAttendant(req, res, next){
   var attendID = parseInt(req.params.id)
   db.Attendant.findById(attendID)
    .then( (attendant) => {
       res.json({
          status: 'success',
          data: attendant.toJSON(),
          message: 'Retrieved requested'
       })
    }).catch((e) => {
       console.error(e)
       return next(e)
    })
}

function getBasestationCode(req, res, next){
   var basestationID = parseInt(req.params.id)
   var newUUID = uuid()
   db.Basestation.update(
     {uuid: newUUID},{where:{id: basestationID}}
   ).then( ( ) => {
       res.status(200)
        .json({
           status: 'success',
           uuid: newUUID,
           message: 'new UUID set'
        }).send()
    }).catch((e) => {
       console.log(e)
       return next(e)
    })
}
