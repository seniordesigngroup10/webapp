![@tend](https://bitbucket.org/seniordesigngroup10/basestation/raw/master/newnew.png )
# webapp #
All the code for the web server and application for the @ tend attendance tacking application.  Runs on JavaScript using [nodejs](https://nodejs.org/en/) and the express framework.  After you `git clone` the directory:

Group Members:

- Benjamin Waechter (benjaminwaechterm@gmail.com)
- Timothy Reardon

## Setup and Dependencies ##
```bash
cd webapp
npm install -g express #May have to be run as sudo 
npm install
```

To run the server locally:

```bash
cd webapp
# one of the following two
node server.js  #runs the server
npm run  #installs dependencies the runs the server
```

## Future Plans ##
We hope to pass this project onto a future group as it has real potential. This is close to something great, just needs a bit of refinement. If you go to WVU and wish to use this a a project further please contact Benjamin Waechter at the email address listed above.